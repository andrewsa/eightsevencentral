This program requests a date and time in yyyy-MM-dd h:mma format, prints out one hour earlier from the input time, sets it to calendar format, and prints out the time zone that of the host machine.

Please see ZIP for complete source code package, design doc, and runnable JAR.