package com.anthonyandrews.eightsevencentral;

import static java.lang.System.out;

import java.util.Scanner;

public class Driver {
	public static void main(String[] args) {
		DateFormatter df = new DateFormatter();
		Scanner keyboard = new Scanner(System.in);
		out.print("Enter date in yyyy-MM-dd h:mma format: ");
		try {
			String date = keyboard.nextLine();
			out.println("One hour earlier: " + df.oneHourEarlier(date));
			out.println("To calendar format: " + df.toCalendarFormat(date));
			out.println("System is in Central Time: " + df.isCentralTime());
			out.println("This system's time zone is: " + df.getTimeZone());
		} catch (IllegalArgumentException e) {
			System.out.println("Incorrect format!");
		} finally {
			try {
				keyboard.close();
			} catch (Exception ignore) {
			}
		}
	}
}