package com.anthonyandrews.eightsevencentral;

import java.util.Calendar;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateFormatter {
	private String amPM;
	private int hour;
	private static DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd h:mma");
	private static Calendar calendar = Calendar.getInstance();
	private static final String TIME_ZONE = calendar.getTimeZone().getID().toString();

	public DateFormatter() {
	}

	public boolean isCentralTime() {
		// Chicago is in central time
		if (TIME_ZONE.equals("America/Chicago")) {
			return true;
		}
		return false;
	}

	public String oneHourEarlier(String dateTimeString) {
		DateTime hourEarlier = format.parseDateTime(dateTimeString).minusHours(1);
		hour = Integer.parseInt(hourEarlier.toString().substring(hourEarlier.toString().indexOf("T") + 1,
				hourEarlier.toString().indexOf(":")));
		if (hour > 12) {
			hour -= 12;
			if (hour == 0) {
				hour = 12;
			}
			amPM = "PM";
		} else {
			if (hour == 0) {
				hour = 12;
			}
			amPM = "AM";
		}
		return formatTime(hourEarlier.toString());
	}

	public String toCalendarFormat(String dateTimeString) {
		DateTime calendarDateTime = format.parseDateTime(dateTimeString);
		StringBuilder stringToReturn = new StringBuilder(calendarDateTime.toString());
		int gmtHour = Integer
				.parseInt(dateTimeString.substring(dateTimeString.indexOf(" ") + 1, dateTimeString.indexOf(":")));

		// Pacific time
		if (TIME_ZONE.equals("America/Los_Angeles")) {
			gmtHour += 8;
		// Mountain time
		} else if (TIME_ZONE.equals("America/Denver")) {
			gmtHour += 7;
		// Central time
		} else if (TIME_ZONE.equals("America/Chicago")) {
			gmtHour += 6;
		// Eastern time
		} else if (TIME_ZONE.equals("America/New_York")) {
			gmtHour += 5;
		}

		if (gmtHour >= 12) {
			gmtHour -= 12;
		}

		if (gmtHour < 10) {
			stringToReturn.replace(stringToReturn.indexOf("T") + 1, stringToReturn.indexOf(":"), "0" + gmtHour);
		} else {
			stringToReturn.replace(stringToReturn.indexOf("T") + 1, stringToReturn.indexOf(":"), "" + gmtHour);
		}
		stringToReturn.replace(stringToReturn.lastIndexOf("-"), stringToReturn.length(), "Z");
		return stringToReturn.toString();
	}

	public String getTimeZone() {
		return TIME_ZONE;
	}

	private String formatTime(String toBeFormatted) {
		StringBuilder formattedDateTime = new StringBuilder(toBeFormatted);
		formattedDateTime.replace(formattedDateTime.indexOf("T"), formattedDateTime.indexOf("T") + 1, " ");
		formattedDateTime.replace(formattedDateTime.indexOf("."), formattedDateTime.length(), "");
		formattedDateTime.replace(formattedDateTime.lastIndexOf(":"), formattedDateTime.length(), amPM);
		formattedDateTime.replace(formattedDateTime.indexOf(" ") + 1, formattedDateTime.indexOf(":"), hour + "");
		return formattedDateTime.toString();
	}
}
